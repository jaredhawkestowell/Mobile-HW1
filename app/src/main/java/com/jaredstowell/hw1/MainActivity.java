package com.jaredstowell.hw1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

double amount, interest, five, ten, fifteen, twenty, twentyfive, thirty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView fiveyr = (TextView)findViewById(R.id.textView10);
        final TextView tenyr = (TextView)findViewById(R.id.textView11);
        final TextView fifteenyr = (TextView)findViewById(R.id.textView12);
        final TextView twentyyr = (TextView)findViewById(R.id.textView13);
        final TextView twentyfiveyr = (TextView)findViewById(R.id.textView14);
        final TextView thirtyyr = (TextView)findViewById(R.id.textView15);



        final EditText inputAmount = (EditText)findViewById(R.id.inputAmount);
        final EditText inputInterest = (EditText)findViewById(R.id.inputInterest);

        Button calculate = (Button)findViewById(R.id.button);

        calculate.setOnClickListener(
                new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        if(inputAmount.getText().toString().isEmpty() | inputInterest.getText().toString().isEmpty()){
                            Toast.makeText(MainActivity.this, "Please enter values!", Toast.LENGTH_LONG).show();
                        }
                        else{
                            amount = Double.parseDouble(inputAmount.getText().toString());
                            interest = Double.parseDouble(inputInterest.getText().toString());
                            interest = interest/100;
                        //System.out.println(amount);
                        //System.out.println(interest);



                                //System.out.println(amount);
                        five = ((interest/12) + (interest/12)/Math.pow((1+(interest/12)),59))*amount;
                        ten = ((interest/12) + (interest/12)/Math.pow((1+(interest/12)),119))*amount;
                        fifteen = ((interest/12) + (interest/12)/Math.pow((1+(interest/12)),179))*amount;
                        twenty = ((interest/12) + (interest/12)/Math.pow((1+(interest/12)),239))*amount;
                        twentyfive = ((interest/12) + (interest/12)/Math.pow((1+(interest/12)),299))*amount;
                        thirty = ((interest/12) + (interest/12)/Math.pow((1+(interest/12)),359))*amount;

                                fiveyr.setText("Five Year: $" + String.format("%.2f", five));
                                //1000fiveyr.Append(String.format("%.2f", five));
                                tenyr.setText("Ten Year: $"+ String.format("%.2f", ten));
                                fifteenyr.setText("Fifteen Year: $" + String.format("%.2f", fifteen));
                                twentyyr.setText("Twenty Year: $" + String.format("%.2f", twenty));
                                twentyfiveyr.setText("Twentyfive Year: $" + String.format("%.2f", twentyfive));
                                thirtyyr.setText("Thirty Year: $" + String.format("%.2f", thirty));


                    }}
                }
        );


    }
        }